local squarePosX = 590     
local squarePosY = 155
local squareSpeed = 0.5
local squareDirection = -1

local playerPosX = 295
local playerPosY = 155

function CheckCollision(x1, y1, width1, height1, x2, y2, width2, height2)
	return x1 < x2 + width2 and
           x2 < x1 + width1 and
           y1 < y2 + height2 and
           y2 < y1 + height1
end

function love.load()
	quitTextBox = false
	love.window.setTitle("Carré :|")
    love.keyboard.setKeyRepeat(true)
    love.window.setMode(640, 360, {resizable=false, vsync=0})
end

function love.draw()
    love.graphics.rectangle("fill", playerPosX, playerPosY, 50, 50)
	love.graphics.rectangle("fill", squarePosX, squarePosY, 50, 50)
end

function love.update(dt)
    if playerPosX > 590 then
        playerPosX = 590
    elseif playerPosX < 0 then
        playerPosX = 0
	elseif playerPosY > 310 then
		playerPosY = 310
	elseif playerPosY < 0 then
		playerPosY = 0
    end
	
	squarePosX = squarePosX + squareSpeed * squareDirection

    if squarePosX < 0 or squarePosX > 590 then
        squareDirection = squareDirection * -1
	end
	
	if CheckCollision(playerPosX, playerPosY, 50, 50, squarePosX, squarePosY, 50, 50) then
		squareDirection = squareDirection * -1
        squarePosX = squarePosX + squareSpeed * squareDirection
	end
end

function love.keypressed(key)
    if key == "left" then
        playerPosX = playerPosX - 15
    elseif key == "right" then
        playerPosX = playerPosX + 15
	elseif key == "up" then
		playerPosY = playerPosY - 15
	elseif key == "down" then
		playerPosY = playerPosY + 15
    end
end
